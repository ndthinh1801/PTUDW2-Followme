(function() {
  'use strict';

  angular
    .module('myNewProject')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('men', {
        url: '/men',
        templateUrl: 'app/main/men.html',
        controller: 'MainController'
      })
      .state('women', {
        url: '/women',
        templateUrl: 'app/main/women.html',
        controller: 'MainController'
      })
      .state('contact', {
        url: '/contact',
        templateUrl: 'app/main/contact.html',
        controller: 'MainController'
      });
      

    $urlRouterProvider.otherwise('/');
  }

})();
