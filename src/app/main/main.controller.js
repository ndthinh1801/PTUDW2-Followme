(function() {
    'use strict';

    var app = angular
        .module('myNewProject')
        .controller('MainController', MainController);
    function PaymentDialogController($scope, $mdDialog, message) {    
      $scope.cardItems = message;
      var total = 0;
      $scope.getTotalPrice = total;
      var timeMillisecond =  (new Date()).getTime();
      var timeStr = toTimeString(timeMillisecond);
      $scope.timeString = timeStr;
      for (var i = 0; i < message.length; i++) {
        total += message[i].price;
        $scope.getTotalPrice = total;
      }
      
      $scope.cancel = function() {
              $mdDialog.cancel();
      };

      $scope.paymentAction = function() {
          var donHang = {
            "madonhang": "donhangx",
            "tinhtrang": "dangcho",
            "tonggiatri": $scope.getTotalPrice,
            "ghichu":$scope.info.ghichu,
            "thongtingiaohang": 'Anh/Chị: ' + $scope.info.hoten + ' SĐT: ' + $scope.info.sdt + ' Địa chỉ: ' + $scope.info.diachi + ' Tổng giá trị: ' + $scope.getTotalPrice ,
            "times": timeMillisecond,
            "sdt": $scope.info.sdt
          };
          var sanPham = [];
          for (var i = 0; i < $scope.cardItems.length; i++) {
            sanPham.push({"tensp": $scope.cardItems[i].name, "gia":$scope.cardItems[i].price, "soluong":1});
          }
          donHang.sanpham = sanPham;
          // Get a key for a new Post.
          var donhangRef = firebase.database().ref().child('aokhoachcm/donhang')
          var newPostKey = donhangRef.push().key;
          donHang.madonhang = newPostKey;
          donhangRef.child(newPostKey).set(donHang);
      };
    }


    function ShowAddProductDialogController($scope, $mdDialog, message) {
            //$scope.sp = sp;
            $scope.isShowImage = false;
            $scope.widthBorder = 5;
            $scope.item = {
              "sex": "Nam"
            };

            $scope.putToFirebase = function()  {
                var item = $scope.item;
                if (item && item.tensp && item.gia && item.giagiam && item.mota && item.sex && item.hinhanh) {
                  var newRef = ""
                  if(item.sex === "Nam") {
                     newRef = firebase.database().ref('aokhoachcm/aokhoacnam/'+item.tensp);
                  } else {
                    newRef = firebase.database().ref('aokhoachcm/aokhoacnu/'+item.tensp);
                  }
                  newRef.set(item);
                  $mdDialog.cancel();
                } else {
          
                }
            };

             $scope.cancel = function() {
              $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
              $mdDialog.hide(answer);
            };

            function updateImageDownload(downloadURL,isShowImage) {
              setTimeout(function() {
                console.log(downloadURL); console.log(isShowImage);
              $scope.$apply(function() {
                  $scope.item.hinhanh = downloadURL;
                  $scope.isShowImage = isShowImage;
                  if (isShowImage) {
                    $scope.widthBorder = 0;  
                  } else {
                    $scope.widthBorder = 5 ;  
                  }
                  
                });
              }, 2000);
            }
            //Start
            $scope.upload = function (file) {
              var uploadTask = storageRef.child('images/' + file.name).put(file);
              uploadTask.on('state_changed', function(snapshot){
                // Observe state change events such as progress, pause, and resume
                // See below for more detail
              }, function(error) {
                // Handle unsuccessful uploads
              }, function() {
                // Handle successful uploads on complete
                // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                var downloadURL = uploadTask.snapshot.downloadURL;
                updateImageDownload(downloadURL,true);
              });
            };
            //End   
    }

    function StatisOrdersDialogController($scope, $mdDialog, message) {
          var imagePath = 'img/list/60.jpeg';
          var donhang = [];
          $scope.disableRadio = true;
          function updateDonHangModel() {
            $scope.donhang = donhang;
            console.log(donhang);
          }

          $scope.cancel = function() {
              $mdDialog.cancel();
            };

          var donhangRef = firebase.database().ref('aokhoachcm/donhang');
          donhangRef.on('value',function(datas){
              datas.forEach(function(data){
                 var childData = data.val();
                 var str =  toTimeString(childData.times);
                 childData.timeString =  str;
                 console.log(str);
                 donhang.push(childData);
              });
              updateDonHangModel();
          });

    }

    function toTimeString(times) {
              console.log(times);
              var timeStr = new Date(times);
              console.log(timeStr);
              var month = timeStr.getMonth() + 1;
              var day  = timeStr.getDay();
              if( day === 0) {
                day = "Chủ nhật";
              } else {
                day = "Thứ " + (day+1);
              }
              var timesT = day + ' - ' +timeStr.getDate() + '-' + month + '-' + timeStr.getFullYear() + 
              ' Lúc: ' + timeStr.getHours() + ':' + timeStr.getMinutes();
              return timesT;
    }
    
    /** @ngInject */
    function MainController($scope, $log, $rootScope,$firebaseObject, $mdDialog,$mdMedia, Upload) {
     
        var vm = $scope;
        var ref = firebase.database().ref();
        var auth = firebase.auth();
        var storageRef = firebase.storage().ref();

      $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
      var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;

      $scope.paymentCard = function(cartItems,ev) {
        // var confirm = $mdDialog.confirm()
        //             .title('Thanh toán')
        //             .textContent('Bạn đã mua hàng xong chưa ? Bước thanh toán này bạn cần nhập một vài thông tin cần thiết nếu còn muốn mua xin hãy mua hết các sản phầm bạn cần rồi thanh toán nhé.')
        //             .ariaLabel('Lucky day')
        //             .ok('Thanh toán')
        //             .cancel('Mua tiếp');

        //       $mdDialog.show(confirm).then(function() {
        //         // Thanh toán
        //         $mdDialog.show({
        //           controller: PaymentDialogController,
        //           templateUrl: 'app/main/template/PaymentDialog.tmpt.html',
        //           locals: {
        //            message: cartItems
        //           },
        //           parent: angular.element(document.body),
        //           targetEvent: ev,
        //           clickOutsideToClose:true,
        //           fullscreen: useFullScreen
        //         })                
        //       }, function() {
        //         // Huỷ
        //       });
              $mdDialog.show({
                  controller: PaymentDialogController,
                  templateUrl: 'app/main/template/PaymentDialog.tmpt.html',
                  locals: {
                   message: cartItems
                  },
                  parent: angular.element(document.body),
                  targetEvent: ev,
                  clickOutsideToClose:true,
                  fullscreen: useFullScreen
                })   
      };

      $scope.showStaticOrderDialog = function(ev) {
        $mdDialog.show({
          controller: StatisOrdersDialogController,
          templateUrl: 'app/main/template/StatisOrdersDialog.tmpt.html',
          locals: {
           message: ""
          },
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: useFullScreen
        })

        $scope.$watch(function() {
          return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
          $scope.customFullscreen = (wantsFullScreen === true);
        });
      }

      $scope.showAddProductDialog = function(ev) {
        $mdDialog.show({
          controller: ShowAddProductDialogController,
          templateUrl: 'app/main/template/AddProductDialog.tmpt.html',
          locals: {
           message: ""
          },
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: useFullScreen
        })

        $scope.$watch(function() {
          return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
          $scope.customFullscreen = (wantsFullScreen === true);
        });
      }
      
      $scope.showDetailProduct = function(ev,it) {
      $scope.sp = it;
        $mdDialog.show({
          controller: ShowDetailDialogController,
          templateUrl: 'app/main/template/ShowDetailProductDialog.tmpt.html',
          locals: {
           sp: $scope.sp,
           isAdmin: $scope.isAdmin
          },
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: useFullScreen
        })
        

        $scope.$watch(function() {
          return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
          $scope.customFullscreen = (wantsFullScreen === true);
        });

        function ShowDetailDialogController($scope,$rootScope, $mdDialog, sp,isAdmin) {
            $scope.sp = sp;
            $scope.isAdmin = isAdmin;
             $scope.cancel = function() {
              $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
              $mdDialog.hide(answer);
            };

            $scope.addToCart = function() {
            $rootScope.cartItems.push({"name":sp.tensp, "thumbnail":sp.hinhanh, "price":sp.giagiam});
            
            $rootScope.count++;
            
            $rootScope.getTotalPrice($rootScope.cartItems);
            $mdDialog.cancel();
          }

            $scope.delete = function() {
            // Appending dialog to dBạnocument.body to cover sidenav in docs app
              var confirm = $mdDialog.confirm()
                    .title('Bạn có thật muốn xoá sản phẩm ?')
                    .textContent('Bạn có thật sự muốn xoá sản phẩm này? Thao tác này sẽ không được khôi phục. Sản phẩm sẽ được xoá vĩnh viễn trên hệ thống!!')
                    .ariaLabel('Lucky day')
                    .targetEvent(ev)
                    .ok('Xoá')
                    .cancel('Huỷ');
              $mdDialog.show(confirm).then(function() {
                // Xoá
                var namRef = firebase.database().ref('aokhoachcm/aokhoacnam/'+$scope.sp.tensp);
                var nuRef = firebase.database().ref('aokhoachcm/aokhoacnu/'+$scope.sp.tensp);

                namRef.remove();
                nuRef.remove();
                window.location.reload();
              }, function() {
                // Huỷ
              });
            };
        }

      };
        
      function googleLogin() {
        auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
      }
      function facebookLogin() {
        auth.signInWithRedirect( new firebase.auth.FacebookAuthProvider());  
      }
      


      var authenticate = [
        {
          //authen: login(),
          authenName: "Sign In With Google",
          icon: "fa-google"
        },
        {
          
          //authen: login(),
          authenName: "Sign In With Facebook",
          icon: "fa-facebook"
        }

      ];

      $scope.iconAuth = "fa-sign-in";
      $scope.authState = "ĐĂNG NHẬP";

      $rootScope.announceClick = function(auth) {
        if (auth.authenName === "Sign In With Google") {
           googleLogin();
        } else if (auth.authenName === "Sign In With Facebook"){
          facebookLogin();
        } else {
          firebase.auth().signOut();
        }
         
      };

      var contact = {
        facebookUrl: "https://www.facebook.com/AoKhoacHCM/?fref=ts",
      };

      var sanphammoi = [];
      var aokhoacnam = [];
      var aokhoacnu = [];

      function updateModel() {
        setTimeout(function() {
          $scope.$apply(function() {
            $scope.sanphammoi = sanphammoi; 
            $scope.aokhoacnu = aokhoacnu;
            $scope.aokhoacnam = aokhoacnam;
          });
        }, 2000);
      }
      $scope.isLogin = false;
      $scope.isAdmin = false;
      function updateAuthenticate() {
        setTimeout(function() {
          $scope.$apply(function() {
            $scope.isLogin = isLogin;
            $scope.isAdmin = isAdmin;
            if(isLogin) {
              $scope.authenticate = [
                {
                  authenName: currentUser.displayName,
                  icon: "fa-user",
                },
               {
                  authenName: "Đăng xuất",
                  icon: "fa-sign-out",
               }
             ];
            // if isAdmin
            if(isAdmin) {
              $scope.role = "QUẢN LÝ";
            } else {
              $scope.role = "HỘI VIÊN";
            }
            $scope.authState = "TÀI KHOẢN";
            } else {
              $scope.authenticate = authenticate;
              $scope.role = "";
              $scope.authState = "ĐĂNG NHẬP";
            }
          });
        }, 2000);
      }
      updateAuthenticate();
      $scope.role = "";

      var aokhoacnuRef = firebase.database().ref('aokhoachcm/aokhoacnu');
      aokhoacnuRef.on('value',function(datas) {
          datas.forEach(function(data) {
             var childData = data.val();
             sanphammoi.push(childData);
             aokhoacnu.push(childData);
          });
          updateModel();
      });

      var aokhoacnamRef = firebase.database().ref('aokhoachcm/aokhoacnam');
      aokhoacnamRef.on('value',function(datas){
          datas.forEach(function(data){
             var childData = data.val();
             sanphammoi.push(childData);
             aokhoacnam.push(childData);
          });
          updateModel();
      });

      vm.authenticate = authenticate;
      vm.contact = contact;
      var commentsRef = firebase.database().ref('aokhoachcm');
      commentsRef.on('value',function(data){
        //console.log(data.val());
      });
      var isLogin = false;
      var isAdmin = false;
      var currentUser = {};
      //googleLogin.addEventListener('click', login, false);
      auth.onAuthStateChanged(function (user) {
        if (user !== null)  {
          currentUser = user;
          console.log('Welcome, ' + user.displayName);
          isLogin = true;
          var userRef = firebase.database().ref('aokhoachcm/users');
          userRef.child(user.uid).on('value',function(snapshot){
            var value =  snapshot.val();
            if(value !== null) {
              if(value.role === 1) {
                console.log("isAdmin");
                isAdmin = true;
              }
              updateAuthenticate();
            } else {
              //console.log("user null");
              var userIDRef = firebase.database().ref('aokhoachcm/users/'+user.uid);
              userIDRef.set({
                'userName': user.displayName,
                'role': 0
              });
            }
          });
        } else {
          isLogin = false;
          isAdmin = false;
        }
        updateAuthenticate();
      });

          $rootScope.cartItems = [];

     
  
          $rootScope.count = 0;
          
          $scope.showCart = false;
          
          $scope.totalPrice = '$20.99';

          
          $rootScope.getTotalPrice = function(items) {
            var total = 0;
            
            //console.log("addClass");

            
            for (var i = 0; i < items.length; i++) {
              total += items[i].price;
            }
            return total;
          }
    }

    app.directive('cartItem', function() {
  
      return {
        restrict: 'E',
        scope: {
          skuName: '@',
          skuImage: '@',
          skuPrice: '@'
        },
        template: 
          "<img ng-src='{{image}}'>" +
          "<button class='add-button center-block' ng-click='addToCart()'>Mua ngay</button>" +
          "<figcaption>{{name}}</figcaption",
        link: function (scope, el, attrs) {
          scope.name = attrs.skuName;
          scope.image = attrs.skuImage;
          scope.price = parseInt(attrs.skuPrice);
        },
        controller: function($scope, $rootScope) {
          
          $scope.addToCart = function() {
            $rootScope.cartItems.push({"name":$scope.name, "thumbnail":$scope.image, "price":$scope.price});
            
            $rootScope.count++;
            
            $rootScope.getTotalPrice($rootScope.cartItems);
          }
          
          $scope.removeFromCart = function() {
            console.log('this item has been removed');
          }
        }
      };
      
    });

      

      $(function() {
    
    $(window).scroll(function() {
      
      var wScroll = $(this).scrollTop();
      
      $('.logo').css({
        'transform' : 'translate(0px, '+ wScroll / 2 + '%)'
      });
      
      $('.back-bird').css({
        'transform' : 'translate(0px, '+ wScroll / 4 + '%)'
      });
      
      $('.fore-bird').css({
        'transform' : 'translate(0px, -'+ wScroll / 40 + '%)'
      });
      
      if (wScroll > ($(window).height() / 1.2)) {
              
        $('.clothes-pics figure').each(function(i) {
          
          setTimeout(function() { 
            $('.clothes-pics figure').eq(i).addClass('is-showing');
          }, 150 * (i+1));
          
        });
        
      }
      
    }); // end scroll
    
  }); // end jquery 

})();

