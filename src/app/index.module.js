(function() {
  'use strict';

  angular
    .module('myNewProject', ['ngAnimate', 'ngCookies', 'ngTouch',
     'ngSanitize', 'ngMessages', 'ngAria',
      'ngResource', 'ui.router', 'firebase',
     'ngMaterial', 'toastr','ui.bootstrap','ngFileUpload']);

})();
